//Author : Rohan Patel
import React, {useState} from "react";
import "./App.css";
import {connect} from "react-redux";
import {viewRestaurantData} from "./Redux/RestaurantUser/Actions/restaurantUserAction";
import RestaurantIndexPage from "./RestaurantSite/Index/RestaurantIndexPage";


function App(props) {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);
    return (
        <div style={{height: "100%"}}>

            <RestaurantIndexPage/>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        restaurantUserObj: state.restaurantUser
    };
};

const mapActionToProps = {
    onDisplayData: viewRestaurantData
};
export default connect(mapStateToProps, mapActionToProps)(App);
