import React, {useState} from "react";
import {connect} from "react-redux";
import axios from "axios";
import {Row, Col} from "reactstrap";
import {addRestaurant, updateDineIn, updateTakeOut} from "../../Redux/RestaurantUser/Actions/restaurantUserAction";
import {baseURL} from "../../baseURL";
import {width} from "@fortawesome/free-solid-svg-icons/faOm";
import {useToasts} from "react-toast-notifications";

const BusinessHours = props => {

    const [mondayStart, setMondayStart] = useState(props.restaurantHours[0].startTime)
    const [mondayEnd, setMondayEnd] = useState(props.restaurantHours[0].endTime)

    const [tuesdayStart, setTuesdayStart] = useState(props.restaurantHours[1].startTime)
    const [tuesdayEnd, setTuesdayEnd] = useState(props.restaurantHours[1].endTime)

    const [wednesdayStart, setWednesdayStart] = useState(props.restaurantHours[2].startTime)
    const [wednesdayEnd, setWednesdayEnd] = useState(props.restaurantHours[2].endTime)

    const [thursdayStart, setThursdayStart] = useState(props.restaurantHours[3].startTime)
    const [thursdayEnd, setThursdayEnd] = useState(props.restaurantHours[3].endTime)

    const [fridayStart, setFridayStart] = useState(props.restaurantHours[4].startTime)
    const [fridayEnd, setFridayEnd] = useState(props.restaurantHours[4].endTime)

    const [saturdayStart, setSaturdayStart] = useState(props.restaurantHours[5].startTime)
    const [saturdayEnd, setSaturdayEnd] = useState(props.restaurantHours[5].endTime)

    const [sundayStart, setSundayStart] = useState(props.restaurantHours[6].startTime)
    const [sundayEnd, setSundayEnd] = useState(props.restaurantHours[6].endTime)

    const [dineIn, setDineIn] = useState(props.valueDineIn)
    const [takeOut, setTakeOut] = useState(props.valueTakeOut)

    const {addToast} = useToasts()

    const onDineInChangeHandler = dineInValue => {
        setDineIn(dineInValue)
        props.updateDineIn(dineInValue);
        axios.put(baseURL + "/restaurant/" + props.resId + "/isDineIn/" + dineInValue).then(res => {
            console.log(res.data)
        }).catch(err => {
            console.error(err)
        })
    }

    const onTakeOutChangeHandler = takeOutValue => {
        setTakeOut(takeOutValue)
        props.updateTakeOut(takeOutValue);
        axios.put(baseURL + "/restaurant/" + props.resId + "/isTakeout/" + takeOutValue).then(res => {
            console.log(res.data)
        }).catch(err => {
            console.error(err)
        })
    }

    const onUpdateHoursClickHandler = () => {

        let updatedHours = [...props.restaurantHours];
        updatedHours[0] = {...updatedHours[0], startTime: mondayStart, endTime: mondayEnd};
        updatedHours[1] = {...updatedHours[1], startTime: tuesdayStart, endTime: tuesdayEnd};
        updatedHours[2] = {...updatedHours[2], startTime: wednesdayStart, endTime: wednesdayEnd};
        updatedHours[3] = {...updatedHours[3], startTime: thursdayStart, endTime: thursdayEnd};
        updatedHours[4] = {...updatedHours[4], startTime: fridayStart, endTime: fridayEnd};
        updatedHours[5] = {...updatedHours[5], startTime: saturdayStart, endTime: saturdayEnd};
        updatedHours[6] = {...updatedHours[6], startTime: sundayStart, endTime: sundayEnd};

        axios.put(baseURL + "/restaurant/" + props.resId + "/businessHours", updatedHours).then(res => {
            if (res.data.code == 0) {
                addToast(res.data.message, {
                    appearance: 'success',
                    autoDismiss: true
                });
            } else {
                addToast("Something went wrong, Please try again later", {
                    appearance: 'error',
                    autoDismiss: true
                });
            }
        }).catch(err => {
            console.error(err)
        })
    }


    return (
        <div className={"container mt-2"}>
            {console.log(dineIn)}
            <Row>
                <Col>
                    <table>
                        <th><label>Add Business Hours</label></th>
                        <tr valign="center">
                            <td><label>Monday</label></td>
                            <td><input type="time" value={mondayStart} onChange={t => setMondayStart(t.target.value)}/>
                            </td>
                            <td><label>&nbsp;to&nbsp;</label></td>
                            <td><input type="time" value={mondayEnd} onChange={t => setMondayEnd(t.target.value)}/></td>
                        </tr>
                        <tr>
                            <td><label>Tuesday</label></td>
                            <td><input type="time" value={tuesdayStart}
                                       onChange={t => setTuesdayStart(t.target.value)}/></td>
                            <td><label>&nbsp;to&nbsp;</label></td>
                            <td><input type="time" value={tuesdayEnd} onChange={t => setTuesdayEnd(t.target.value)}/>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Wednesday</label></td>
                            <td><input type="time" value={wednesdayStart}
                                       onChange={t => setWednesdayStart(t.target.value)}/></td>
                            <td><label>&nbsp;to&nbsp;</label></td>
                            <td><input type="time" value={wednesdayEnd}
                                       onChange={t => setWednesdayEnd(t.target.value)}/></td>
                        </tr>
                        <tr>
                            <td><label>Thursday</label></td>
                            <td><input type="time" value={thursdayStart}
                                       onChange={t => setThursdayStart(t.target.value)}/></td>
                            <td><label>&nbsp;to&nbsp;</label></td>
                            <td><input type="time" value={thursdayEnd} onChange={t => setThursdayEnd(t.target.value)}/>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Friday</label></td>
                            <td><input type="time" value={fridayStart} onChange={t => setFridayStart(t.target.value)}/>
                            </td>
                            <td><label>&nbsp;to&nbsp;</label></td>
                            <td><input type="time" value={fridayEnd} onChange={t => setFridayEnd(t.target.value)}/></td>
                        </tr>
                        <tr>
                            <td><label>Saturday</label></td>
                            <td><input type="time" value={saturdayStart}
                                       onChange={t => setSaturdayStart(t.target.value)}/></td>
                            <td><label>&nbsp;to&nbsp;</label></td>
                            <td><input type="time" value={saturdayEnd} onChange={t => setSaturdayEnd(t.target.value)}/>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Sunday</label></td>
                            <td><input type="time" value={sundayStart} onChange={t => setSundayStart(t.target.value)}/>
                            </td>
                            <td><label>&nbsp;to&nbsp;</label></td>
                            <td><input type="time" value={sundayEnd} onChange={t => setSundayEnd(t.target.value)}/></td>
                        </tr>
                        <tr>
                            <button className="primaryButton" onClick={() => onUpdateHoursClickHandler()}>Update Hours
                            </button>
                        </tr>
                    </table>
                </Col>
                <Col>
                    <labe><b>Order Type</b></labe>
                    <br/><br/>

                    <label>Dine In</label>&nbsp;
                    <input type="checkbox" {...(dineIn ? {checked: true} : {checked: false})}
                           onChange={event => onDineInChangeHandler(event.target.checked)}/><br/>

                    <label>Take Out</label>&nbsp;
                    <input type="checkbox" {...(takeOut ? {checked: true} : {})}
                           onChange={(e) => onTakeOutChangeHandler(e.target.checked)}/>

                </Col>
            </Row>

            {/*<Row>*/}
            {/*    <Col xs={3}>{props.day}</Col>*/}
            {/*    <Col xs={4} sm={2}><input type={"time"} style={{width: '100px'}}/> </Col>*/}
            {/*    <Col xs={4} sm={2}><input type={"time"} style={{width: '100px'}}/></Col>*/}
            {/*    <Col xs={0}/>*/}
            {/*</Row>*/}
        </div>

    )
};


const mapStateToProps = state => {
    return {
        restaurantHours: state.restaurantUser.restaurant.businessHours,
        resId: state.restaurantUser.restaurant.id,
        valueDineIn: state.restaurantUser.restaurant.dineIn,
        valueTakeOut: state.restaurantUser.restaurant.takeout
    };
};

const mapActionsToProps = {
    updateDineIn: updateDineIn,
    updateTakeOut: updateTakeOut
};


export default connect(mapStateToProps, mapActionsToProps)(BusinessHours);
