//Author : Rohan Patel

import React from "react";
import classNames from "classnames";
import {Container} from "reactstrap";
import NavBar from "./ContentNavBar";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MenuCategory from "../../Containers/Menu/Category/MenuCategory";
import MenuAddItems from "../../Containers/Menu/AddItems/MenuAddItems";
import ViewMenu from "../../Containers/Menu/ViewMenu/ViewMenu";
import AddTable from "../../Containers/Table/AddTable/AddTable";
import ViewOrders from "../../Containers/Orders/ViewAllOrders/ViewOrders";
import RestaurantDetails from "../../Containers/Restaurant/RestaurantDetails/RestaurantDetails";
import BGImage from "../../Containers/Restaurant/BGImage/ResBgImage"

export default props => (
    <Container
        fluid
        className={classNames("content", {"is-open": props.isOpen})}
    >
        <NavBar toggle={props.toggle} history={props.history}
                restaurantUserUpdate={props.restaurantUserUpdate}/>

        <Switch>
            <Route exact path="/menu/category" component={MenuCategory}/>
            <Route exact path="/menu/addItems" component={MenuAddItems}/>
            <Route exact path="/menu/editItem" component={MenuAddItems}/>
            <Route exact path="/menu/view" component={ViewMenu}/>
            <Route exact path="/restaurant/addTable" component={AddTable}/>
            <Route exact path="/restaurant/viewOrders" component={ViewOrders}/>
            <Route exact path="/restaurant/details" component={RestaurantDetails}/>
        </Switch>
    </Container>
);
