//Author : Rohan Patel

import React, {useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAlignLeft} from "@fortawesome/free-solid-svg-icons";
import {
    Navbar,
    Button,
    NavbarToggler,
    Collapse,
    Nav,
    NavItem,
    NavLink
} from "reactstrap";

export default props => {
    const [isOpen, setOpen] = useState(true);
    const toggle = () => setOpen(!isOpen);

    const onLogoutPressHandler = () => {
        props.restaurantUserUpdate({})
        props.history.push("/login")

    }
    return (
        <Navbar
            color="light"
            light
            className="navbar shadow-sm p-3 mb-5 bg-white rounded"
            expand="md"
        >
            <Button style={{backgroundColor: "#0033CC"}} onClick={props.toggle}>
                <FontAwesomeIcon icon={faAlignLeft}/>
            </Button>
            <NavbarToggler onClick={toggle}/>
            <Collapse isOpen={isOpen} navbar>
                <Nav className="ml-auto" navbar>
                    {/*<NavItem>*/}
                    {/*  <NavLink tag={Link} to={"/menu"}>*/}
                    {/*    Menu*/}
                    {/*  </NavLink>*/}
                    {/*</NavItem>*/}
                </Nav>
                <button className="btn btn-danger" onClick={() => onLogoutPressHandler()}>Log Out</button>
            </Collapse>
        </Navbar>
    );
};
