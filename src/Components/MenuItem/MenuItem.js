//Author : Rohan Patel

import React, {useState} from 'react'
import {Card, Row, Col, CardText, CardTitle, CardBody, CardImg, CardSubtitle} from "reactstrap";
import {faPen, faTrash, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Link} from "react-router-dom";
import MenuAddItems from "../../Containers/Menu/AddItems/MenuAddItems";

const MenuItem = props => {
    const d = props.dish;
    return (
        <div className={"preview-container shadow bg-white mt-4"}>
            <Card>
                <CardImg top width="100%"
                         src={d.image}
                         alt="Card image cap"/>
                <CardBody>
                    <CardTitle><b>{d.name}</b></CardTitle>
                    <CardSubtitle><i className="text-muted">{d.description}</i></CardSubtitle>
                    <br/>
                    <CardText>
                        Price : ${d.price} &nbsp;&nbsp;&nbsp;&nbsp; Tax : {d.tax}%
                    </CardText>
                    <Row>
                        <Col>
                            <Link to={{
                                pathname: "/menu/editItem", state: {
                                    dish: d
                                }
                            }}>
                                <button className={"ternaryButton"}
                                        style={{paddingLeft: '20px', paddingRight: '20px'}}
                                >Edit<FontAwesomeIcon
                                    className={"editIcon ml-2"}
                                    icon={faPen}
                                    size={"md"}
                                /></button>
                            </Link>
                        </Col>
                        <Col>
                            <button className={"dangerButton"}
                                    style={{paddingLeft: '17px', paddingRight: '17px'}}
                                    onClick={() => props.onDeleteClickHandler({
                                        categoryId: d.categoryId,
                                        dishId: d.id
                                    })}>Delete <FontAwesomeIcon
                                className={"editIcon ml-2"}
                                icon={faTrash}
                                size={"md"}
                            /></button>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </div>
    );
};
export default MenuItem;