//Author : Rohan Patel

import React from 'react';
import {Row, Col} from "reactstrap";

const OrderItem = props => {
    const item = props.item;
    return (
        <div className="mt-2">
            <Row>
                <Col lg={6} md={6}>
                    <Row><Col><span className={"font-weight-bold"}>{item.dish.name}</span></Col></Row>
                    <Row><Col><span className={"text-muted"}>
                        {item.note.trim() !== "" && <i>" {item.note} "</i>}
                        </span></Col></Row>
                </Col>
                <Col>
                    <span>{item.quantity}</span>
                </Col>
                <Col>
                    <span>${item.dish.price.toFixed(2)}</span>
                </Col>
            </Row>
        </div>
    );
};

export default OrderItem;