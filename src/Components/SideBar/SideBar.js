//Author : Rohan Patel

import React from "react";
import Style from "./SideBarStyle.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {connect} from "react-redux";
import {
    faHome,
    faAddressCard,
    faUtensils,
    faChair,
    faStroopwafel
} from "@fortawesome/free-solid-svg-icons";
import SubMenu from "./SubMenu";
import {NavItem, NavLink, Nav, NavbarBrand} from "reactstrap";
import classNames from "classnames";
import {Link} from "react-router-dom";

const SideBar = props => (
    <div className={classNames("sidebar", {"is-open": props.isOpen})}>
        <div className="sidebar-header mt-4 mb-5">
      <span color="info" onClick={props.toggle} style={{color: "black"}}>
        &times;
      </span>
            <center>
                <NavbarBrand href="/restaurant">WayOrder</NavbarBrand>
            </center>
        </div>
        <div className="side-menu">
            <div className="restaurant-name-holder">{props.restaurantUser.restaurant.name}</div>
            <br/>
            <br/>
            <Nav vertical className="list-unstyled pb-3">
                <SubMenu title="Menu" icon={faUtensils} items={submenus[1]}/>
                <SubMenu title="Orders" icon={faStroopwafel} items={submenus[3]}/>
                <SubMenu title="Tables" icon={faChair} items={submenus[2]}/>
                <NavItem>
                    <NavLink tag={Link} to={"/restaurant/details"}>
                        <FontAwesomeIcon icon={faAddressCard} className="mr-2"/>
                        Restaurant details
                    </NavLink>
                </NavItem>
            </Nav>
        </div>
    </div>
);

const submenus = [
    [
        {
            title: "Home 1",
            target: "/menu/category"
        },
        {
            title: "Home 2",
            target: "Home-2"
        },
        {
            title: "Home 3",
            target: "Home-3"
        }
    ],
    [
        {
            title: "Category",
            target: "/menu/category"
        },
        {
            title: "Add Items",
            target: "/menu/addItems"
        },
        {
            title: "View Menu",
            target: "/menu/view"
        }
    ],
    [
        {
            title: "Add table", target: "/restaurant/addTable"
        }
    ],
    [
        {
            title: "View orders", target: "/restaurant/viewOrders"
        }
    ]
];

const mapStateToProps = state => {
    return {
        restaurantUser: state.restaurantUser
    };
};

const mapActionsToProps = {};
export default connect(mapStateToProps, mapActionsToProps)(SideBar);
