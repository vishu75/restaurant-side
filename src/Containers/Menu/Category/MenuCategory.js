//Author : Rohan Patel

import React, {useEffect, useRef, useState} from "react";
import {connect} from "react-redux";
import Styles from "./MenuCategoryStyle.css";
import {useToasts} from 'react-toast-notifications';
import {confirmAlert} from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import axios from "axios";
import {
    CustomInput,
    Row,
    Col,
    InputGroup,
    Label,
    Input,
    FormGroup
} from "reactstrap";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPen, faTrash, faTimes} from "@fortawesome/free-solid-svg-icons";
import {baseURL} from "../../../baseURL";
import {addCategory, deleteCategory, updateCategory} from "../../../Redux/RestaurantUser/Actions/restaurantUserAction";

const MenuCategory = props => {

    let sc = 0;
    let scn = ""
    try {
        sc = props.categories[0].id;
        scn = props.categories[0].name;
    } catch (e) {
        console.error("No pre existed categories found");
    }

    const [editClicked, setEditClicked] = useState(false);
    const [toggleNewCategory, setToggleNewCategory] = useState(false);
    const [selectedCategory, setSelectedCategory] = useState(sc); // category id selected
    const [categoryName, setCategoryName] = useState(scn); // name of the category selected / updated / new
    const {addToast} = useToasts()

    const onSelectedCategoryChangeHandler = value => {
        setSelectedCategory(value);
        props.categories.forEach(c => {
            if (c.id == value) setCategoryName(c.name);
        });
    };

    const onUpdateButtonClickHandler = () => {
        axios.post(baseURL + "/restaurant/" + selectedCategory + "/menu/category?categoryName=" + categoryName).then(res => {
            if (res.data.code === 0) {
                props.updateCategory(res.data.object);
                addToast(res.data.message, {
                    appearance: 'success',
                    autoDismiss: true
                })
            }
        }).catch(err => {
            console.error(err);
        });
    };

    const onDeleteButtonClickHandler = () => {

        confirmAlert({
            title: 'Confirm delete',
            message: 'Are you sure to delete category ' + categoryName + " ?",
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        axios.delete(baseURL + "/restaurant/" + props.restaurantId + "/menu/category?categoryId=" + selectedCategory).then(res => {
                            console.log(res.data);
                            if (res.data.code === 0) {
                                props.deleteCategory(selectedCategory);
                                addToast(res.data.message, {
                                    appearance: 'success',
                                    autoDismiss: true
                                })
                            }
                        }).catch(err => {
                            console.error(err);
                        });
                    }
                },
                {
                    label: 'No',
                    onClick: () => {
                    }
                }
            ]
        });
    };

    const onAddButtonClickHandler = () => {
        const categoryObj = {
            name: categoryName,
            dishes: []
        };
        axios.put(baseURL + "/restaurant/" + props.restaurantId + "/menu/category", categoryObj).then(res => {
            if (res.data.code === 0) {
                props.addNewCategory(res.data.object);
                addToast(res.data.message, {
                    appearance: 'success',
                    autoDismiss: true
                })
            }
        }).catch(err => {
            console.log(err);
        });
    };

    return (
        <div>
            <span>Category name</span>
            <br/>
            <br/>
            <Row>
                <Col lg={8} md={10} sm={12} xs={12}>
                    <FormGroup row>
                        <Col lg={10} md={10} sm={8} xs={8}>
                            <CustomInput
                                type="select"
                                name="category"
                                id="category"
                                inline={true}
                                onChange={e => onSelectedCategoryChangeHandler(e.target.value)}
                            >
                                {props.categories.map(c => {
                                    return (
                                        <option key={c.id} value={c.id}>
                                            {c.name}
                                        </option>
                                    );
                                })}
                            </CustomInput>
                        </Col>
                        <Col>
                            <div
                                style={{
                                    display: "flex",
                                    height: "100%",
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}
                                onClick={() => setEditClicked(!editClicked)}
                            >
                                <FontAwesomeIcon
                                    className={"editIcon"}
                                    icon={editClicked ? faTimes : faPen}
                                    size={"lg"}
                                    style={editClicked ? {color: "red"} : {}}
                                />
                            </div>
                        </Col>
                        <Col>
                            <div
                                style={{
                                    display: "flex",
                                    height: "100%",
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}

                                onClick={() => onDeleteButtonClickHandler()}
                            >
                                <FontAwesomeIcon
                                    className={"editIcon"}
                                    icon={faTrash}
                                    size={"lg"}
                                />
                            </div>
                        </Col>
                    </FormGroup>
                </Col>
            </Row>
            {editClicked && (
                <Row>
                    <Col lg={9} md={12} sm={12} xs={12}>
                        <FormGroup row>
                            <Col lg={9} md={5} sm={8} xs={7}>
                                <Input
                                    type="text"
                                    value={categoryName}
                                    onChange={e => setCategoryName(e.target.value)}
                                />
                            </Col>
                            <Col>
                                <button class="ternaryButton" onClick={() => onUpdateButtonClickHandler()}>Update
                                </button>
                            </Col>
                        </FormGroup>
                    </Col>
                </Row>
            )}
            <br/>
            <br/>

            <button
                class="secondaryButton"
                onClick={() => setToggleNewCategory(!toggleNewCategory)}
                style={
                    toggleNewCategory
                        ? {backgroundColor: "red", color: "white", borderColor: "red"}
                        : {}
                }
            >
                Create new category
            </button>
            <br/>
            {toggleNewCategory && (
                <Row className={"mt-3"}>
                    <Col lg={9} md={12} sm={12} xs={12}>
                        <FormGroup row>
                            <Col lg={9} md={5} sm={8} xs={7}>
                                <Input type="text" placeholder="New category name"
                                       onChange={(e) => setCategoryName(e.target.value)}/>
                            </Col>
                            <Col>
                                <button class="primaryButton" onClick={() => onAddButtonClickHandler()}>Add</button>
                            </Col>
                        </FormGroup>
                    </Col>
                </Row>
            )}
        </div>
    );
};

const mapStateToProps = state => {
    return {
        categories: state.restaurantUser.restaurant.menu.categories,
        restaurantId: state.restaurantUser.restaurant.id
    };
};

const mapActionsToProps = {
    addNewCategory: addCategory,
    updateCategory: updateCategory,
    deleteCategory: deleteCategory
};
export default connect(mapStateToProps, mapActionsToProps)(MenuCategory);
