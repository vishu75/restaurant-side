//Author : Rohan Patel

import React, {useEffect, useRef, useState} from "react";
import {connect} from "react-redux";
import Styles from "./ViewMenuStyle.css";
import {useToasts} from 'react-toast-notifications';
import {confirmAlert} from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import axios from "axios";
import ReactStars from 'react-stars';
import {
    CustomInput,
    Row,
    Col,
    InputGroup,
    Label,
    FormText,
    Input,
    FormGroup,
    InputGroupAddon,
    InputGroupText,
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle
} from "reactstrap";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPen, faTrash, faTimes} from "@fortawesome/free-solid-svg-icons";
import {baseURL} from "../../../baseURL";
import {addCategory, deleteCategory, updateCategory} from "../../../Redux/RestaurantUser/Actions/restaurantUserAction";
import MenuItem from "../../../Components/MenuItem/MenuItem";
import {BrowserRouter} from "react-router-dom";
import MenuAddItems from "../AddItems/MenuAddItems";

const ViewMenu = props => {


    const [menu, setMenu] = useState({"categories": []});
    const [dishes, setDishes] = useState([]);
    const [rerender, setRerender] = useState(0);
    const [edit, setEdit] = useState(false);
    const {addToast} = useToasts();


    useEffect(() => {
        axios.get(baseURL + "/restaurant/" + props.restaurantId + "/menu").then(res => {
            if (res.data.code == 0) {
                try {
                    const dishList = res.data.object.categories[0].dishes
                    for (let j = 0; j < dishList.length; j++) {
                        dishList[j] = {...dishList[j], categoryId: res.data.object.categories[0].id};
                    }
                    setDishes(dishList);
                } catch (e) {
                    alert("No dish found");
                }

                setMenu(res.data.object);
            }
        }).catch(err => {
            console.log(err);
        });

    }, [rerender]);


    const onCategoryChangeHandler = value => {
        console.log("onCategoryChangeHandler clicked");
        for (let i = 0; i < menu.categories.length; i++) {
            if (menu.categories[i].id == value) {
                const dishList = menu.categories[i].dishes
                for (let j = 0; j < dishList.length; j++) {
                    dishList[j] = {...dishList[j], categoryId: value};
                }
                console.log(dishList);
                setDishes(dishList)
                break;
            }
        }
    };

    const onViewAllButtonClickHandler = () => {
        let dishList = [];
        menu.categories.forEach(c => {
            c.dishes.forEach(d => {
                dishList.push({...d, categoryId: c.id});
            });
        });
        console.log(dishList);
        setDishes(dishList);
    };

    const onDeleteClickHandler = ({categoryId, dishId}) => {
        confirmAlert({
            title: 'Confirm delete',
            message: 'Are you sure to delete the item ? ',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        axios.delete(baseURL + "/restaurant/" + props.restaurantId + "/menu/item?categoryId=" + categoryId + "&dishId=" + dishId).then(res => {
                            console.log(res.data);
                            for (let i = 0; i < dishes.length; i++) {
                                if (dishes[i].id == dishId) {
                                    dishes.splice(i, 1);
                                    break;
                                }
                            }
                            addToast(res.data.message, {
                                appearance: 'success',
                                autoDismiss: true
                            });
                            setRerender(rerender + 1);
                        }).catch(err => {
                            console.log(err);
                        });
                    }
                },
                {
                    label: 'No',
                    onClick: () => {
                    }
                }
            ]
        });

    };

    const onEditClickHandler = () => {
        setEdit(true);
    };
    return (
        <div>
            {edit && <MenuAddItems edit={true}/>}
            {!edit && <div>
                <Label className="text-muted">Select category to view the items</Label>
                <Row>
                    <Col lg={6} md={6}>
                        <Input type={"select"} className={"mt-2"}
                               onChange={(e) => onCategoryChangeHandler(e.target.value)}>
                            {menu.categories.map(c => {
                                return <option key={c.id} value={c.id}>{c.name}</option>
                            })}
                        </Input>
                    </Col>
                    <Col>
                        <button title="Click to view all menu items in restaurant" className="secondaryButton mt-2"
                                onClick={() => onViewAllButtonClickHandler()}>View all
                        </button>
                    </Col>
                </Row>

                <Row className="mt-5">
                    {dishes.map(d => {
                        return (
                            <Col lg={3} md={6}>
                                <div className={"preview-container shadow bg-white"}>
                                    <MenuItem dish={d} onDeleteClickHandler={onDeleteClickHandler}
                                              onEditClickHandler={onEditClickHandler}/>
                                </div>
                            </Col>
                        );
                    })}

                </Row>
            </div>}
        </div>
    );
};

const mapStateToProps = state => {
    return {
        categories: state.restaurantUser.restaurant.menu.categories,
        restaurantId: state.restaurantUser.restaurant.id
    };
};

const mapActionsToProps = {
    addNewCategory: addCategory,
    updateCategory: updateCategory,
    deleteCategory: deleteCategory
};
export default connect(mapStateToProps, mapActionsToProps)(ViewMenu);
