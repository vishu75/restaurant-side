//Author : Rohan Patel

import React, {useEffect, useState, useRef} from 'react'
import {connect} from 'react-redux';
import OrderCard from "../../../Components/OrderCard/OrderCard";
import {Row, Col} from "reactstrap";
import axios from "axios";
import Styles from "./ViewOrdersStyle.css";
import {baseURL} from "../../../baseURL";
import OrderItem from "../../../Components/OrderItem/OrderItem";
import Pusher from 'pusher-js';
import {useToasts} from 'react-toast-notifications';
import moment from "moment-timezone";

const ViewOrders = props => {

    const [orders, setOrders] = useState([]);
    const orderRef = useRef();
    orderRef.current = orders;
    const [currentOrder, setcurrentOrder] = useState({orderItems: []});
    const {addToast} = useToasts()

    useEffect(() => {
        const pusher = new Pusher('a15e9f068cfceb6d6e26', {
            cluster: 'us2'
        });

        const channel = pusher.subscribe('restaurant-channel' + props.restaurantId);
        channel.bind('orderPlaced', pusherHandler);

        const url = baseURL + "/order/restaurant/" + props.restaurantId;
        axios.get(url).then(res => {
            console.log(res.data);
            setOrders(res.data.object);


        }).catch(err => {
            console.error(err);
        });


    }, []);


    const pusherHandler = data => {
        console.log("PUSHER CALLED");
        console.log("NEW ORDER : ")
        console.log(data)
        setOrders([data, ...orderRef.current]);
        addToast("New order received, Go to orders page to view it", {
            appearance: 'info',
            autoDismiss: true
        });
    }

    const onOrderClickHandler = o => {
        setcurrentOrder(o);
    };
    const tConvert = time => {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1); // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original str ing
    }


    return (
        <div>
            <Row>
                <Col lg={7} md={7}>
                    <div className="scrollContainer">
                        <Row className="p-3 m-2 bg-dark rounded">
                            <Col className="text-white">Order #</Col>
                            <Col className="text-white">Customer name</Col>
                            <Col className="text-white">Date / Time</Col>
                            <Col className="text-white">Item count</Col>
                        </Row>
                        {
                            orders.map(o => {
                                const d = new Date(
                                    moment.utc(o.orderPlacedTime).tz("America/Toronto").format().slice(0,-6)
                                );
                                const formatedTime = d.toDateString()+
                                    "\n"+d.toLocaleTimeString();
                                o.orderPlacedTime = formatedTime
                                return <div key={o.id} onClick={() => onOrderClickHandler(o)}><OrderCard order={o}/>
                                </div>
                            })
                        }
                    </div>
                </Col>
                <Col>
                    <Row className={"mb-4"}><Col lg={6} md={6}>Item</Col><Col>Quantity</Col><Col>Price</Col></Row>
                    {
                        currentOrder.orderItems.map(i => {
                            return <OrderItem key={i.id} item={i}/>
                        })
                    }
                    <div className="mt-3">
                        <span className={"text-success font-weight-bolder"}>Total : ${currentOrder.total}</span>
                    </div>
                    <div className="mt-3">
                        <span className={"text-success font-weight-bolder"}>Tax : ${currentOrder.tax}</span>
                    </div>
                    <div className="mt-3">
                        <span
                            className={"text-success font-weight-bolder"}>Net Total : ${(currentOrder.total + currentOrder.tax).toFixed(2)}</span>
                    </div>
                    <div className="mt-3">
                        {currentOrder.isDineIn != null &&
                        <div>
                            <span>DINE IN ORDER</span><br/>
                            <span>TIME : {tConvert(currentOrder.isDineIn.time)}</span><br/>
                            {currentOrder.isDineIn.seatingArrangement.map(item=>{

                                return <div>
                                    <span>TABLE : {item.table.number} </span>&nbsp;&nbsp;&nbsp;
                                    <span>PARTY SIZE : {item.partySize}</span>
                                </div>
                            })
                            }
                        </div>
                        }
                        {currentOrder.isInRestaurantOrder != null &&
                        <div>
                            <span>TABLE ORDER</span><br/>
                            <span>Table # : {currentOrder.isInRestaurantOrder.number}</span>
                        </div>
                        }
                        {currentOrder.isTakeOutOrder != null &&
                        <div>
                            <span>TAKEOUT ORDER</span><br/>
                            <span>Date : {currentOrder.isTakeOutOrder.date}</span><br/>
                            <span>Time : {tConvert(currentOrder.isTakeOutOrder.time)}</span>
                        </div>
                        }
                    </div>
                </Col>
            </Row>

        </div>
    );
}

const mapStateToProps = state => {
    return {
        restaurantId: state.restaurantUser.restaurant.id
    }
}

const mapActionsToProps = {}
export default connect(mapStateToProps, mapActionsToProps)(ViewOrders);