//Author : Jay Patel
import React, {useState} from "react";
import styles from "./ResBgImage.css";
import {storage} from "../../../firebase/firebase";
import {
    Dropdown,
    Form,
    Input
} from "reactstrap";
import {v4 as uuidv4} from 'uuid';
import {connect} from "react-redux";
import axios from "axios";
import {baseURL} from "../../../baseURL";
import {useToasts} from "react-toast-notifications";

const ResBgImage = props => {
    const {addToast} = useToasts()

    const allInputs = {imgUrl: ''}
    const [imageAsFile, setImageAsFile] = useState('')
    const [imageAsUrl, setImageAsUrl] = useState(allInputs)

    const handleImageAsFile = (e) => {


        const image = e.target.files[0]
        setImageAsFile(imageFile => (image))

        // document.getElementById("newavatar").src = document.getElementById("logoFile").value;

    }

    const handleFireBaseUpload = e => {

        e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if (imageAsFile === '') {
            console.error(`not an image, the image file is a ${typeof (imageAsFile)}`)
        } else {
            const fileName = uuidv4();
            const extension = "." + imageAsFile.name.split(".")[1];
            const uploadTask = storage.ref('/logos/' + props.restaurantId + "/" + fileName + extension).put(imageAsFile)
            //initiates the firebase side uploading
            uploadTask.on('state_changed',
                (snapShot) => {
                    //takes a snap shot of the process as it is happening
                    console.log(snapShot)
                }, (err) => {
                    //catches the errors
                    console.log(err)
                }, () => {
                    // gets the functions from storage refences the image storage in firebase by the children
                    // gets the download url then sets the image from firebase as the value for the imgUrl key:
                    storage.ref('logos/' + props.restaurantId).child(fileName + extension).getDownloadURL()
                        .then(fireBaseUrl => {
                            setImageAsUrl(prevObject => ({...prevObject, imgUrl: fireBaseUrl}))
                            console.log("FIREBASE URL IS : " + fireBaseUrl)
                            uploadImage(fireBaseUrl)
                        })
                })
        }
    }


    const uploadImage = url => {
        console.log("URL IS : " + url);
        axios.put(baseURL + "/restaurant/" + props.restaurantId + "/logo?logoUrl=" + encodeURI(url))
            .then(res => {
                if (res.data.code == 0) {
                    console.log(res.data);
                    addToast(res.data.message, {
                        appearance: 'success',
                        autoDismiss: true
                    });
                }
            }).catch(err => {
            console.log(err);
        })
    };
    return (
        <>
            <div>
                {/* this is forclass="rounded-circle z-depth-1-half avatar-pic" Background Image*/}
                <img src={imageAsUrl.imgUrl} className={"restaurant-image"}/>
                <Form onSubmit={handleFireBaseUpload} id="formtoupload">
                    <Input type="file" className="inputUpload" name="file" accept=".jpeg,.jpg,.png" type="file"
                           onChange={handleImageAsFile}/>
                    <button class="btn1" className="btn1" id="logoFile">Upload a Image</button>
                </Form>


            </div>

        </>
    );
};


const mapStateToProps = state => {
    return {
        restaurantId: state.restaurantUser.restaurant.id
    };
};

const mapActionsToProps = {};

export default connect(mapStateToProps, mapActionsToProps)(ResBgImage);