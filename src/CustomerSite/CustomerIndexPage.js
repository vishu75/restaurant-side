//Author : Vishvakumar Mavani

import React, {Component, useState} from "react";
import {Navbar, NavbarBrand} from "reactstrap";
import "./CustomerIndexPage.css";
import {Row, Col} from "react-bootstrap";
import CustomerPageNavBar from "./NavBar/CustomerPageNavBar";
import * as Phone from "phone";
import axios from "axios";
import {baseURL} from "../baseURL";

const CustomerIndexPage = props => {
    const [phoneNumber,setPhoneNumber] = useState("");
    const onNumberChangeHandler = text=>{
            if (text.length <= 10 && Number(text)!="NaN") {
                setPhoneNumber(text);
            }
    };
    const onGetAppClickHandler = _=>{
        console.log("GET APP CLICKED"+phoneNumber);
        let res = Phone(phoneNumber);
        console.log(res);
        if (res.length !== 0) {
            axios.post(baseURL+"/api/message",phoneNumber).then(res=>{
                console.log(res.data);
                if(res.data.message.length!=0){
                    alert("Check your phone ;)");
                }else{
                    alert("Oops, cannot reach you");
                }
            }).catch(err=>{ alert("Oops, cannot react you");});
        } else {
            alert("Incorrect phone number");
        }
    };
    return (
        <div>
            <div className="circle"></div>
            <CustomerPageNavBar history={props.history}/>
            <div className={"container"}>
                <Row>
                    <Col className="leftContent" sm={7}>
                        <h1 className="title">No More Wait For Your Favourite Dish</h1>
                        <p className="para">
                            Have your dish ready to be served instantly upon your restaurant arrival.
                        </p>
                        <input className="inputPhone" type="text" placeholder="Phone Number"
                        value={phoneNumber} onChange={(e)=>onNumberChangeHandler(e.target.value)}/>

                        <button className="getApp" onClick={()=>onGetAppClickHandler()}>Get App</button>

                        <div className="images">

                            <a href="https://www.apple.com/ca/ios/app-store/">
                                <img src="./appstore.png" height="50" width="155"/>
                            </a>
                            <a href="https://play.google.com/store?hl=en">
                                <img src="./playstore.png" height="50" width="185" style={{paddingLeft: '20px'}}/>
                            </a>
                        </div>

                    </Col>
                    <Col className="rightContent">
                        <img className="dish" src="./dish.png" height="350"/>
                    </Col>
                </Row>
            </div>
        </div>
    );
}
export default CustomerIndexPage;