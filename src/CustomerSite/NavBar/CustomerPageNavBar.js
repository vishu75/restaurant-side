import React, {useState} from "react";
import {Navbar, NavbarToggler, Collapse, Nav, NavItem, NavLink, NavbarBrand} from "reactstrap";
import style from "./CustomerNabBarStyle.css"

const CustomerPageNavBar = props => {

    const [open, setOpen] = useState(false);

    return (
        <Navbar className={"container"} color={"transparent"} light expand="md">
            <NavbarBrand href="/">WayOrder</NavbarBrand>
            <NavbarToggler onClick={() => setOpen(!open)} style={{color: 'black'}}/>
            <Collapse isOpen={open} navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <button className={"nav-restaurant-button"}
                                onClick={() => props.history.push("/restaurant")}>For Restaurants
                        </button>
                    </NavItem>
                </Nav>
            </Collapse>
        </Navbar>

    )
};

export default CustomerPageNavBar;