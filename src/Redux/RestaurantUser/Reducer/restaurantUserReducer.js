//Author : Rohan Patel
import {
    ADD_CATEGORY,
    ADD_RESTAURANT, ADD_TABLE,
    DELETE_CATEGORY, DELETE_TABLE,
    UPDATE_CATEGORY, UPDATE_DINEIN, UPDATE_TAKEOUT,
    VIEW_RESTAURANT_DATA
} from "../Actions/actionTypes";

const initialState = {};
const restaurantReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case ADD_RESTAURANT:
            return payload;
        case ADD_CATEGORY:
            state.restaurant.menu.categories.push(payload);
            return JSON.parse(JSON.stringify(state));
        case UPDATE_CATEGORY:
            for (let i = 0; i < state.restaurant.menu.categories.length; i++) {
                if (state.restaurant.menu.categories[i].id === payload.id) {
                    state.restaurant.menu.categories[i] = payload;
                    break;
                }
            }
            return JSON.parse(JSON.stringify(state));
        case DELETE_CATEGORY :
            console.log(payload);
            for (let i = 0; i < state.restaurant.menu.categories.length; i++) {
                if (state.restaurant.menu.categories[i].id == payload) {
                    state.restaurant.menu.categories.splice(i, 1);
                    break;
                }
            }
            console.log(state);
            return JSON.parse(JSON.stringify(state));

        case ADD_TABLE:
            state.restaurant.tables.push(payload);
            return JSON.parse(JSON.stringify(state));
        case DELETE_TABLE:
            for (let i = 0; i < state.restaurant.tables.length; i++) {
                if (state.restaurant.tables[i].id == payload) {
                    state.restaurant.tables.splice(i, 1);
                    break;
                }
            }
            return JSON.parse(JSON.stringify(state));
        case UPDATE_DINEIN:
            state.restaurant.dineIn = payload;
            return state;
        case UPDATE_TAKEOUT:
            state.restaurant.takeout = payload;
            return state;
        default:
            return state;
    }
};

export default restaurantReducer;
