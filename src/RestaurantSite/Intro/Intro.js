//Author : Jay Patel
import React from "react"
import Fade from "react-reveal"
import "./Intro.css";


export default function Intro() {
    return (
        <div className="main" id="intro">
            <div className="intro-main-div">
                <Fade left duration={100000}>
                    <div className="intro-image-div">
                        <img alt="Food Ordering" src={require("../../assests/images/IntroImage.svg")}></img>
                    </div>
                </Fade>
                <Fade right duration={1000}>
                    <div className="intro-text-div">
                        <h1 className="intro-heading">Product and Services</h1>
                        <p className="intro-text-subtitle">We provide restaurant’s a revolutionary mobile app by which
                            the restaurant becomes completely ready to start taking orders digitally from the table
                            itself.</p>
                        {/* <SoftwareSkill />
            <div>
              {introSection.intro.map(intro => {
                return <p className="subTitle intro-text">{intro}</p>;
              })}
            </div> */}
                        <ul className="intro-text-subtitle">
                            <li>Customer can modify the dish according to their preference without communicating with
                                the waiter
                            </li>
                        </ul>
                        <ul className="intro-text-subtitle">
                            <li>All in one reward system to give customer loyalty every time they visit the restaurant
                            </li>
                        </ul>
                        <ul className="intro-text-subtitle">
                            <li>Our mobile app allows customer to place an order in no time.</li>
                        </ul>
                    </div>
                </Fade>
            </div>
        </div>
    );
}
  