//Author : Jay Patel
import React, {useState, useEffect} from "react";
import styles from "./Loader.css"
import {Row, Col} from "reactstrap";

const Loader = props => {

    useEffect(() => {
        const timer = setTimeout(() => {
            props.history.push("/dashboard");
        }, 2000);
        return () => props.history.push("/dashboard");
    }, []);

    return (
        <>
            <div className="loader">
                <div></div>
                <div></div>
                <div></div>
            </div>

        </>

    )
};

export default Loader
