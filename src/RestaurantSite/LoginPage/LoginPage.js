//Author : Vishvakumar Mavani
import React, {useState, useEffect} from "react";
import "./LoginPageStyle.css";
import RestaurantPageNavBar from "../NavBar/RestaurantPageNavBar";
import {Row, Col} from "react-bootstrap";
import CustomInputField from "../../Components/SignupInputField/CustomInputField";
import Lottie from 'react-lottie';
import cooking from "../../assests/images/cookingGirlLottie.json";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEye, faEyeSlash} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import {baseURL} from "../../baseURL";
import {Alert} from "reactstrap";
import {addRestaurant} from "../../Redux/RestaurantUser/Actions/restaurantUserAction";
import {connect} from "react-redux";
import Loader from "../Loader/Loader"
import DashboardScreen from "../../Screens/DashboardScreen/DashboardScreen"

const LoginPage = props => {


    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [valid, setValid] = useState(false);
    const [passwordRevel, setPasswordRevel] = useState(false);

    const [serverError, setServerError] = useState("");

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: cooking,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };

    const onEmailChange = val => {
        setEmail(val);
    };
    const onPasswordChange = val => {
        setPassword(val);
    }
    const checkValid = () => {
        if (email !== "" && password !== "")
            return true
        return false
    }

    const onLoginButtonClickHandler = _ => {
        setServerError("");
        axios
            .post(baseURL + "/user/restaurant/login", {
                email: email,
                password: password
            })
            .then(res => {
                if (res.data === "")
                    setServerError("Oops, you might have misspelled something")
                else {
                    props.onAddRestaurantUser(res.data);
                    console.log("Login Successfull")
                    props.history.push("/loader")

                }
            })
            .catch(err => {
                console.log(err);
            });
    };

    const onEnterKeyPress = e=>{
      if(e.key === "Enter"){
          onLoginButtonClickHandler();
      }
    };

    return (
        <div className={"container"}>
            <RestaurantPageNavBar displayLogin={false}/>
            <Row>
                <Col className={"login-left-col"} xs={12} md={12} lg={5}>
                    <span className={"login-title"}>Welcome Back</span><br/>
                    {serverError != "" &&
                    <span style={{textAlign: 'center', color: 'red', fontWeight: '700'}}>{serverError}</span>
                    }
                    <Row>
                        <Col><CustomInputField placeholder={"Email"} type={"email"}
                                               onChange={(val) => onEmailChange(val)}/></Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className="input-group">
                                <CustomInputField placeholder={"Password"}
                                                  onChange={(val) => onPasswordChange(val)}
                                                  type={passwordRevel ? "text" : "password"}
                                onEnterKeyPress={(e)=>onEnterKeyPress(e)}/>
                                <button
                                    className="show-pwd"
                                    onClick={() => setPasswordRevel(!passwordRevel)}
                                >
                                    <FontAwesomeIcon
                                        icon={passwordRevel ? faEyeSlash : faEye}
                                    />
                                </button>
                            </div>
                        </Col>
                    </Row>
                    <Row style={{margin: '20px 10px 10px 5px'}}>
                        <Col xs={1}><input type={"checkbox"}/></Col>
                        <Col> <span className={"terms-privacy-text"}>Remember Me</span></Col>
                    </Row>


                    <Row>
                        <Col>
                            <button className={"login-button"} style={checkValid() ? {} : {opacity: 0.5}}
                                    disabled={valid}
                                    onClick={() => onLoginButtonClickHandler()}

                            >
                                Login
                            </button>
                        </Col>
                    </Row>
                    <span className={"terms-privacy-text"}>Don't have an account ? <a href="/register"
                                                                                      style={{color: '#364EB8'}}>
                        Register</a></span>
                </Col>
                <Col lg={7} className={"login-right-col"}>
                    {/*<img src={require("../../assests/cooking-bro.svg")} className={"img-bro"}/>*/}
                    <Lottie options={defaultOptions}
                            height={600}
                            width={600}
                    />
                </Col>
            </Row>
        </div>
    )
};


const mapStateToProps = state => {
    return {
        restaurantUser: state.restaurantUser
    };
};

const mapActionToProps = {
    onAddRestaurantUser: addRestaurant
};
export default connect(mapStateToProps, mapActionToProps)(LoginPage);

