//Author : Rohan Patel
import React, {useState} from "react";
import {Navbar, NavbarToggler, Collapse, Nav, NavItem, NavLink, NavbarBrand} from "reactstrap";
import style from "./RestaurantNabBarStyle.css"

const RestaurantPageNavBar = props => {

    const [open, setOpen] = useState(false);

    return (
        <Navbar className={"container"} color={"transparent"} light expand="md">
            <NavbarBrand href="/restaurant">WayOrder</NavbarBrand>
            <NavbarToggler onClick={() => setOpen(!open)} style={{color: 'black'}}/>
            <Collapse isOpen={open} navbar>
                <Nav className="ml-auto" navbar>
                    {props.displayLogin && <NavItem>
                        <button className={"nav-login-button"}
                                onClick={() => props.history.push("/login")}>Login
                        </button>
                    </NavItem>
                    }
                </Nav>
            </Collapse>
        </Navbar>

    )
};

export default RestaurantPageNavBar;