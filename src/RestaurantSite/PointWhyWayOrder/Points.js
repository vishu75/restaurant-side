//Author : Jay Patel
import React from "react"
import Fade from "react-reveal"
import {Col, Row} from "reactstrap";
import "./Points.css";


export default function Points() {
    return (
        <div className={"container mt-5 mb-5"}>
            <span className={"why-us-title"}>Why WayOrder ?</span><br/>
            <Row className={"ml-5 mt-5 row1"}>
                <Col>
                    <Row>
                        <Col md={4} lg={3}>
                            <img alt="Food Ordering-point"
                                 src={require("../../assests/images/smartphone.svg")}
                                 className={"phone-img"}/>
                        </Col>
                        <Col>
                            <p className="why-us-heading">Easy to get Started</p>
                            <p className="why-us-subtitle">
                                Start getting orders from customer's mobile phone/tablet with couple of taps!
                            </p>
                        </Col>
                    </Row>
                </Col>
                <Col>
                    <Row>
                        <Col md={4} lg={3}>
                            <img alt="Food Ordering-point"
                                 src={require("../../assests/images/gift.svg")}
                                 className={"gift-img"}/>
                        </Col>
                        <Col>
                            <p className="why-us-heading">Sweet Rewards</p>
                            <p className="why-us-subtitle">
                                Give can earn and redeem WayOrder rewards towards any restaurant they love
                            </p>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className={"ml-5 mt-5 row2"}>
                <Col>
                    <Row>
                        <Col md={4} lg={3}>
                            <img alt="Food Ordering-point"
                                 src={require("../../assests/images/fast-time.svg")}
                                 className={"time-img"}/>
                        </Col>
                        <Col>
                            <p className="why-us-heading">Save precious time</p>
                            <p className="why-us-subtitle">
                                Helps customer to save time by taking orders and payments from phone
                            </p>
                        </Col>
                    </Row>
                </Col>
                <Col>
                    <Row>
                        <Col md={4} lg={3}>
                            <img alt="Food Ordering-point"
                                 src={require("../../assests/images/clipboard.svg")}
                                 className={"clip-img"}/>
                        </Col>
                        <Col>
                            <p className="why-us-heading">Accurate order</p>
                            <p className="why-us-subtitle">
                                No more cunfussion between customer and order taker. Take orders efficiently from the
                                app
                            </p>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    );
}

/*
<div className="main-point" id="intro-point">
            <div className="intro-main-div-point">
                <div className="intro-text-div-point">
                    <h1 className="intro-heading-point">Why WayOrder?</h1>
                </div>
                <p>

                    <div class="container">
                        <div class="row justify-content-md-center">
                            <div class="col" className="intro-image-div-logo-point">
                                <img alt="Food Ordering-point"
                                     src={require("../../assests/images/smartphone.svg")}></img>
                            </div>
                            <div class="col">
                                <p className="intro-text-div-phone-point">Easy to get Started</p>
                                <p className="intro-heading-subtitle-point">Start getting orders
                                    from <br></br> customer's mobile phone/tablet <br></br> with couple of taps! </p>


                            </div>
                            <div class="col" className="intro-image-div-logo-point">
                                <img alt="Food Ordering-point" src={require("../../assests/images/gift.svg")}></img>
                            </div>
                            <div class="col">
                                <p className="intro-text-div-phone-point">Sweet Rewards</p>
                                <p className="intro-heading-subtitle-point">Give can earn and redeeem<br></br>WayOrder
                                    rewards towards any <br></br> restaurant they love</p>


                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row justify-content-md-center">
                            <div class="col" className="intro-image-div-logo-point">
                                <img alt="Food Ordering-point"
                                     src={require("../../assests/images/fast-time.svg")}></img>
                            </div>
                            <div class="col">
                                <p className="intro-text-div-phone-point">Easy to get Started</p>
                                <p className="intro-heading-subtitle-point">Start getting orders
                                    from <br></br> customer's mobile phone/tablet <br></br> with couple of taps! </p>


                            </div>
                            <div class="col" className="intro-image-div-logo-point">
                                <img alt="Food Ordering-point"
                                     src={require("../../assests/images/clipboard.svg")}></img>
                            </div>
                            <div class="col">
                                <p className="intro-text-div-phone-point">Easy to get Started</p>
                                <p className="intro-heading-subtitle-point">Start getting orders
                                    from <br></br> customer's mobile phone/tablet <br></br> with couple of taps! </p>


                            </div>
                        </div>
                    </div>
                </p>

            </div>
 */