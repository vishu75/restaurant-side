//Author : Jay Patel
import React from "react"
import Fade from "react-reveal"
import {Row, Col} from "reactstrap";
import "./QRCodeService.css";


export default function QRCodeService() {
    return (
        <div className="container mt-5">
            <Row>
                <Col className={"qr-right-col"}>
                    <img alt="Food Ordering" src={require("../../../assests/images/QR_Code.jpg")} className={"qr-img"}/>
                </Col>
                <Col xs={12} md={6} lg={6} className={"qr-left-col"}>
                    <h1 className="intro-heading">Table Ordering System</h1>
                    <p className="intro-text-subtitle">Customer can easily browse, choose,
                        order and pay for their favourite dishes right from their comfortable
                        table by scanning a simple QR code</p>
                </Col>

            </Row>


        </div>
    );
}
  