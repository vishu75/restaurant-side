//Author : Rohan Patel
import React, {useState} from "react";
import RestaurantPageNavBar from "../NavBar/RestaurantPageNavBar";
import {Row, Col, Alert} from "reactstrap";
import "./SignupPageStyle.css"
import CustomInputField from "../../Components/SignupInputField/CustomInputField";
import rocket from "../../assests/images/rocketBro.json";
import Lottie from "react-lottie";
import {faEye, faEyeSlash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import axios from "axios";
import {baseURL} from "../../baseURL";
import * as Phone from "phone";


const SignupPage = props => {

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [resName, setResName] = useState("");
    const [phone, setPhone] = useState("");
    const [invalidPhone, setInvalidPhone] = useState(false);
    const [resAddress, setResAddress] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [passwordRevel, setPasswordRevel] = useState(false);
    const [serverError, setServerError] = useState("");
    const [serverSuccess, setServerSuccess] = useState("");

    const onFirstNameChange = val => setFirstName(val.trim());
    const onLastNameChange = val => setLastName(val.trim());
    const onResNameChange = val => setResName(val.trim());
    const onPhoneChange = val => {
        let res = Phone(val);
        console.log(res);
        if (res.length !== 0) {
            setInvalidPhone(false);
            setPhone(val.trim());
        } else
            setInvalidPhone(true);
    }
    const onResAddressChange = val => setResAddress(val.trim());
    const onEmailChange = val => setEmail(val.trim());
    const onPasswordChange = val => setPassword(val.trim());

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: rocket,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };


    const onRegisterButtonClickHandler = () => {
        setServerError("")
        setServerSuccess("")
        if (true) {
            const restaurantObject = {
                name: resName,
                unit: Number(22),
                street: resAddress,
                city: "TEST",
                postal: "L5W1P5",
                phone: phone
            };
            const restaurantUserObject = {
                email: email,
                password: password
            };

            const holder = {
                restaurant: restaurantObject,
                restaurantUser: restaurantUserObject
            };

            axios
                .put(baseURL + "/restaurant/register", holder)
                .then(res => {
                    const resData = res.data;
                    if (resData === 1) {
                        // Restaurant already exist
                        setServerError("Oops, Looks like we already have same info on system")
                    }
                    if (resData === 2) {
                        //User already exists
                        setServerError("Oops, Looks like we already have same info on system")
                    }
                    if (resData === 3) {
                        //Restaurant and user registered
                        setServerSuccess("Hurray, Welcome onboard !")
                    }
                })
                .catch(err => {
                    console.log(err);
                    console.log("Somethign went wrong")
                });
        }
    };


    const checkValid = () => {
        if (firstName !== "" && lastName !== "" && resName !== ""
            && phone !== "" && resAddress !== ""
            && email !== "" && password !== "")
            return true
        return false
    };


    return (
        <div className={"container"}>
            <RestaurantPageNavBar/>
            <Row>
                <Col className={"signup-left-col"} xs={12} md={12} lg={5}>
                    <span className={"signup-title"}>One Click To Boost Your Sales</span><br/>
                    {serverError != "" &&
                    <span style={{textAlign: 'center', color: 'red', fontWeight: '700'}}>{serverError}</span>
                    }
                    {serverSuccess != "" &&
                    <span style={{textAlign: 'center', color: 'green', fontWeight: '700'}}>{serverSuccess}</span>
                    }
                    <Row>
                        <Col><CustomInputField placeholder={"First Name"} type="text"
                                               onChange={val => onFirstNameChange(val)}/></Col>
                        <Col><CustomInputField placeholder={"Last Name"} type="text"
                                               onChange={val => onLastNameChange(val)}/></Col>
                    </Row>
                    <Row>
                        <Col><CustomInputField placeholder={"Restaurant Name"} type="text"
                                               onChange={val => onResNameChange(val)}/></Col>
                        <Col>
                            <CustomInputField placeholder={"Phone Number"} type="tel"
                                              onChange={val => onPhoneChange(val)}
                                              error={invalidPhone}/></Col>

                    </Row>
                    <Row>
                        <Col><CustomInputField placeholder={"Restaurant Address"} type="text"
                                               onChange={val => onResAddressChange(val)}/></Col>
                    </Row>
                    <Row>
                        <Col><CustomInputField placeholder={"Email"} type="email" onChange={val => onEmailChange(val)}/></Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className="input-group">
                                <CustomInputField placeholder={"Password"}
                                                  onChange={(val) => onPasswordChange(val)}
                                                  type={passwordRevel ? "text" : "password"}/>
                                <button
                                    className="show-pwd"
                                    onClick={() => setPasswordRevel(!passwordRevel)}
                                >
                                    <FontAwesomeIcon
                                        icon={passwordRevel ? faEyeSlash : faEye}
                                    />
                                </button>
                            </div>
                        </Col>
                    </Row>
                    <span className={"terms-privacy-text"}>By Signing up, you confirm that you accept our
                        <span style={{color: '#364EB8'}}> Terms of Use</span> and <span style={{color: '#364EB8'}}>Privacy Policy</span></span>
                    <Row>
                        <Col>
                            <button className={"signup-button"} onClick={() => onRegisterButtonClickHandler()}
                                    style={checkValid() ? {} : {opacity: 0.5}}
                            >
                                Sign Up
                            </button>
                        </Col>
                    </Row>
                    <span className={"terms-privacy-text"}>Already have an account ?
                        <a href="/login" style={{color: '#364EB8'}}> Login</a></span>
                </Col>
                <Col lg={7} className={"signup-right-col"}>
                    {/*<img src={require("../../assests/images/Market launch-pana.svg")} className={"img-rocket"}/>*/}
                    <Lottie options={defaultOptions}
                            height={750}
                            width={700}
                    />
                </Col>
            </Row>
        </div>
    )
};

export default SignupPage;