//Author : Rohan Patel
import React, {useState, useEffect} from "react";
import Style from "./DashboardScreenStyle.css";
import SideBar from "../../Components/SideBar/SideBar";
import Content from "../../Components/Content/Content";
import {connect} from "react-redux";
import {addRestaurant} from "../../Redux/RestaurantUser/Actions/restaurantUserAction";

import {ToastProvider} from 'react-toast-notifications';

import {BrowserRouter, Route, Switch} from "react-router-dom";

const DashboardScreen = props => {
    const [isOpen, setOpen] = useState(true);
    const toggle = () => setOpen(!isOpen);

    useEffect(() => {
        // if(Object.keys(props.restaurantUser).length === 0){
        //   console.log("NO KEYS")
        //   props.history.push("/login");
        // }else {
        //   console.log("SOME KEYS");
        // }
    });
    return (
        <BrowserRouter>
            <div className="App wrapper">
                <ToastProvider placement={"top-center"}>
                    <SideBar toggle={toggle} isOpen={isOpen}/>
                    <Content toggle={toggle} history={props.history} isOpen={isOpen}
                             restaurantUserUpdate={props.onAddRestaurantUser}/>
                </ToastProvider>
            </div>
        </BrowserRouter>
    );
};

const mapStateToProps = state => {
    return {
        restaurantUser: state.restaurantUser
    };
};

const mapActionToProps = {
    onAddRestaurantUser: addRestaurant
};
export default connect(mapStateToProps, mapActionToProps)(DashboardScreen);
