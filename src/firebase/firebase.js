import firebase from 'firebase/app'
import 'firebase/storage'

let firebaseConfig = {
    apiKey: "AIzaSyCMNxu8unt0v_fVh_65iNRz3fp4YHoRkQc",
    authDomain: "wayorderapp.firebaseapp.com",
    databaseURL: "https://wayorderapp.firebaseio.com",
    projectId: "wayorderapp",
    storageBucket: "wayorderapp.appspot.com",
    messagingSenderId: "266469264864",
    appId: "1:266469264864:web:f6025a4da181be2232af85",
    measurementId: "G-TQV44GKMGS"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


const storage = firebase.storage()

export {
    storage, firebase as default
}

