//Author : Rohan Patel
import React from "react";
import {BrowserRouter, Route, Redirect} from "react-router-dom";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.css";
import {createStore, combineReducers} from "redux";
import {Provider} from "react-redux";
import restaurantReducer from "./Redux/RestaurantUser/Reducer/restaurantUserReducer";
import DashboardScreen from "./Screens/DashboardScreen/DashboardScreen";
import CustomerIndexPage from "./CustomerSite/CustomerIndexPage";
import SignupPage from "./RestaurantSite/SignupPage/SignupPage";
import LoginPage from "./RestaurantSite/LoginPage/LoginPage";
import LoaderPage from "./RestaurantSite/Loader/Loader";
import RestaurantIndexPage from "./RestaurantSite/Index/RestaurantIndexPage";
import Error404 from "./Error404";
import axios from "axios";
import jwt from "jsonwebtoken";

// const app = express();
// app.use(cors());
// Add a request interceptor
const secretKey = "792F423F4528482B4D6251655468576D5A7134743777217A24432646294A404E";
const issuerId = "462948404D635166546A576E5A7234753778214125442A472D4B614E645267556B58703273357638792F423F4528482B4D6251655368566D597133743677397A";
const token = jwt.sign({data:'WayOrder',iss:issuerId},secretKey);
axios.interceptors.request.use(config=> {
    // Do something before request is sent
    console.log(token);

    config.headers.Authorization =  token;
    return config;
});

const allReducers = combineReducers({
    restaurantUser: restaurantReducer
});

const store = createStore(
    allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const ROUTING = (
    <Provider store={store}>
        <BrowserRouter>
            <div>
                <Route exact path="/" component={CustomerIndexPage}/>
                <Route exact path="/register" component={SignupPage}/>
                {/*<Route exact path="/login" component={LoginScreen}/>*/}
                <Route exact path="/loader" component={LoaderPage}/>
                <Route exact path="/login" component={LoginPage}/>

                <Route exact path="/dashboard" component={DashboardScreen}/>
                <Route exact path="/restaurant" component={RestaurantIndexPage}/>
                {/*<Route exact path="/404" component={Error404} />*/}
                {/*<Redirect to="/404" />*/}

            </div>
            ``
        </BrowserRouter>
    </Provider>
);
ReactDOM.render(ROUTING, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
